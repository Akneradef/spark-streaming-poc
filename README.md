# spark-streaming-exercises

This project provides a set of utils in order to develop different exercises using Spark Streaming.

Currently, the project contains only a log event generator and basic Spark Streaming processor.

## Event Generator

The Event Generator produces a series of log events in plain text through common OS sockets. The idea behind this
component is to simulate two sources of log information based on the contents of the classical auth.log and access.log.
The generator is configured by specifying the number of iterations to go through, and the number of success and failed
log entries per iteration. This allows the listening Spark Streaming to develop different solutions to
determine which traffic should trigger an alarm.

To use the event generator with the default options execute:

    mvn compile exec:java -Dexec.mainClass=org.spark.examples.events.EventGenerator

Additionally , you can launch the generator with custom options.

    mvn compile exec:java -Dexec.mainClass=org.spark.examples.events.EventGenerator -Dexec.args="-numberIterations 20 -numberEventsOK 20 -numberEventsFail 80 -sleepTime 50"

When the generator is launched, it will wait for two clients to connect. Clients connecting to port _10011_ will receive
authorization entries. Clients connecting to port _10012_ will receive access log entries. Notice that the generator will
not send any information unless both clients are connected.

To facilitate the processing, each log entry has the fields separated by _";"_

### Auth events

An auth event is composed of the following fields:

  - Timestamp
  - Source host
  - Process
  - Message

### Web events

A web event is composed of the following fields:

  - Source host
  - Timestamp
  - Method
  - URL
  - HTTP code

## Streaming Log Processor

The Log processor is simple Spark Streaming application which is configured to consume two streams from two sockets
and convert plain text messages into the Datasets of objects of Scala Case Classes. Datasets then processed to 
simulate basic data collection of such sample information as:

 1) Number of access attempts from particular host which is over some threshold;
 2) Number of success and number of failed authentication requests;

Both metrics are collecting over the sliding window with window duration of 10 seconds and slide duration of 5 seconds.

# Resources

The following resources may help during the coding:

 - [Spark Streaming Programming Guide](https://spark.apache.org/docs/latest/streaming-programming-guide.html)
 - [Spark Streaming Examples](https://github.com/apache/spark/tree/master/examples/src/main/scala/org/apache/spark/examples/streaming)
 - [Spark Programming Guide](https://spark.apache.org/docs/latest/programming-guide.html)

