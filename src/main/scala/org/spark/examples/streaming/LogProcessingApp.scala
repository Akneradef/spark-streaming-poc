/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.spark.examples.streaming

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Row, SparkSession}

import java.sql.Timestamp

/**
 * Of PoC spark streaming processor
 */
object LogProcessingApp {

  /**
   * Field separator.
   */
  val Separator = ";";

  /**
   * Threshold that determines when a number of failed auth entries is considered an attack.
   */
  val ThresholdAuth = 1;

  /**
   * Threshold that determines when a number of failed web access entries is considered an attack.
   */
  val ThresholdWeb = 10;

  def main(args : Array[String]) {
    //Suppress Spark output
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)

    val spark = SparkSession
      .builder
      .master("local[*]")
      .appName("EventProcessor")
      .getOrCreate()

    import spark.implicits._

    val authEventDF = spark.readStream
      .format("socket")
      .option("host", "localhost")
      .option("port", 10011)
      .load()

    val webEventDF = spark.readStream
      .format("socket")
      .option("host", "localhost")
      .option("port", 10012)
      .load()

    val authTimeWindow = window($"timestamp", "10 seconds", "5 seconds")
    val webTimeWindow = window($"timestamp", "10 seconds", "5 seconds")

    val authObjectDF = authEventDF.map(row => getAuthEventFromRow(row))

    val authByWindowQuery = authObjectDF
      .writeStream
      .foreachBatch((dataset, epochId) =>{
        val successAuthDF = dataset
          .filter($"msg".contains("OK"))
          .groupBy(authTimeWindow)
          .count().as("success_auth_count")

        val failedAuthDF = dataset
          .filter($"msg".contains("fail"))
          .groupBy(authTimeWindow)
          .count().as("failed_auth_count")

        val authStatDF = successAuthDF.as("s")
          .join(failedAuthDF.as("f"), $"s.window"===$"f.window")
          .select($"s.window", $"s.count".as("success_count"), $"f.count".as("failure_count"))

        authStatDF.show(false)
      })
      .start()

    val webObjectDF = webEventDF.map(row => getWebEventFromRow(row))

    val webRequestsByHostWindow = webObjectDF
      .groupBy(webTimeWindow, $"host")
      .count()
      .filter($"count" > ThresholdWeb)
      .writeStream
      .format("console")
      .outputMode("complete")
      .option("truncate","false")
      .start()

    authByWindowQuery.awaitTermination()
    webRequestsByHostWindow.awaitTermination()
  }

  def getAuthEventFromRow(row: Row): AuthEvent ={
    val strRow = row(0).toString.split(Separator)
    val timestamp = Timestamp.valueOf(strRow(0))
    new AuthEvent(timestamp,strRow(1),strRow(2),strRow(3))
  }

  def getWebEventFromRow(row: Row): WebEvent ={
    val strRow = row(0).toString.split(Separator)
    val timestamp = Timestamp.valueOf(strRow(1))
    new WebEvent(strRow(0),timestamp,strRow(2),strRow(3),strRow(4).toInt)
  }

}
