package org.spark.examples.streaming

import java.sql.Timestamp

/**
 * Auth Event.
 */
object AuthEvent {

}

case class AuthEvent(timestamp: Timestamp,
                     host: String,
                     protocol: String,
                     msg: String)

