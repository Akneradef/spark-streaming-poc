package org.spark.examples.streaming

import java.sql.Timestamp

/**
 * Web Event.
 */

object WebEvent {

}

case class WebEvent(host: String,
                    timestamp: Timestamp,
                    method: String,
                    address: String,
                    responseCode: Int)